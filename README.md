# Single-container Mastodon

Don't ever host things anywhere but on a domain that you own.

This violates all the docker best practices and puts the whole-ass app
inside a single container (including redis and postgres!) so that you can
easily bring up a self-hosted mastodon on things like caprover self-hosted
PaaS in a single "app" without having to worry about multiple containers or
configuring cross-container links.

This is scratching an itch: I really just wanted a one-command single-user
Mastodon instance that stores all of its state in a single directory I can
tar and scp.

# Features

* Ubuntu bionic
* Everything in one container, including minimal postfix, postgres,
  and redis
* assets are precompiled on server startup
* all state is in one volume/dir
* database migrations can be run at startup

# Build Variables

* `UID` - numeric userid for `mastodon` user that everything runs as
  (default: `991`)
* `GID` - numeric groupid for `mastodon` user that everything runs as
  (default: `991`)
* `REPO_URL`: url to repository to use (default:
  `https://github.com/tootsuite/mastodon.git`)
* `REPO_REV`: git revision to use (default:
  `c4118ba71ba31e408c02d289e111326ccc6f6aa2`, mastodon v3.0.1)

# Environment Variables

* `RUN_DB_MIGRATIONS` (default: `true`, set to empty string `""` to disable)
* `SIDEKIQ_WORKERS`: (default: `5`)
* others: https://github.com/tootsuite/mastodon/blob/master/.env.production.sample

# Volumes

* `/state` - everything lives in here, db, redis, static files, logs,
  everything

Note that `/state/envdir` is an environment-variable-defining directory that
is used for all service invocations, so you can set environment settings
here for all processes without restarting the docker container.

# Ports

* `3000`: mastodon web (no tls, do that elsewhere)
* `4000`: mastodon streaming

# Credits

All praise to wonderfall's earlier single-container mastodon:

* https://github.com/Wonderfall/docker-mastodon
* https://hub.docker.com/r/wonderfall/mastodon

(I only made this because it did not include postfix/postgres/redis.)

# Author

Jeffrey Paul &lt;sneak@sneak.berlin&gt;

# Example captain-definition

For use with Caprover.  

(Note that the NetData system monitor container that ships with CapRover is
**spyware**, and CapRover [refuses to address that they are embedding
spyware](https://github.com/caprover/caprover/issues/553), and the container
maintainer [refuses to patch out the
spyware](https://github.com/titpetric/netdata/pull/91) so it is advisable
that you do not enable NetData when using CapRover unless you would like
your usage data and IP and a unique identifier silently transmitted to
Google.)

```
{
  "schemaVersion": 2,
  "imageName": "sneak/mastodon:v3.0.1"
}
```

# After First Run

```
# docker exec -ti <container name> /bin/bash
root@c81a376bf546:~/app# su -l --preserve-environment mastodon
mastodon@c81a376bf546:~$ cd app
mastodon@c81a376bf546:~/app$ RAILS_ENV=production envdir /state/envdir \
     bin/tootctl accounts create <newusername> \
     --email <your email> \
     --confirmed \
     --role admin
```

If `WEB_DOMAIN` != `LOCAL_DOMAIN`, make sure you have both set in your
shell's environment (or in the envdir) before running tootctl to add your
user.  If you are running within a PaaS like Caprover or Heroku and you set
them in the GUI, they should be defined on the container itself and already
in your shell (and stay there with `--preserve-environment`).  You can check
by running `env` before running `bin/tootctl`.
