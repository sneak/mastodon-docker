NAME := sneak/mastodon

default: build

build:
	docker build -t $(NAME) .

run:
	-docker rm -f mastodon
	docker run --env WEB_DOMAIN="test123.example.com" --hostname mastodon --name mastodon -ti $(NAME)

deploy:
	caprover deploy -a mastodon --default
