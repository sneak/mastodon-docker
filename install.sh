#!/bin/bash

export PATH="$HOME/.rbenv/bin:$PATH"
export RUBY_CONFIGURE_OPTS="--with-jemalloc"

git clone https://github.com/rbenv/rbenv.git $HOME/.rbenv
cd $HOME/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.bashrc
echo 'eval "$(rbenv init -)"' >> $HOME/.bashrc
git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
eval "$(rbenv init -)"
rbenv install 2.6.5
rbenv global 2.6.5
gem update --system
gem install bundler --no-document

git clone $REPO_URL /mastodon/app
cd /mastodon/app
git checkout $REPO_REV

bundle config build.nokogiri --use-system-libraries
bundle install \
  -j$(getconf _NPROCESSORS_ONLN) \
  --deployment --without development test
yarn install --pure-lockfile --ignore-engines
