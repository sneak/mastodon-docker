FROM phusion/baseimage:0.11

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# make some SSH keys that will never get used just so that
# the container doesn't generate new ones each and every boot
# (sshd is not enabled in the container)
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

ENV \
    RUN_DB_MIGRATIONS=true \
    SIDEKIQ_WORKERS=5 \
    RAILS_SERVE_STATIC_FILES=true \
    RAILS_ENV=production \
    NODE_ENV=production \
    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/mastodon/app/bin

ARG UID=991
ARG GID=991

# this repo master maps to upstream master, branches in this repo
# map to specific upstream commits that are released as versions
ARG REPO_URL=https://github.com/tootsuite/mastodon.git
ARG REPO_REV=master

# install os prereq stuff to image
ADD prereqs.sh /tmp/prereqs.sh
RUN bash /tmp/prereqs.sh

# install mastodon:
ADD install.sh /tmp/install.sh

WORKDIR /mastodon

ENV HOME /mastodon

ENV BIND 0.0.0.0

RUN chpst -u mastodon:mastodon bash /tmp/install.sh

ADD ./rootfs /

RUN chmod +x /etc/service/postfix/run
RUN chmod +x /etc/service/redis/run
RUN chmod +x /etc/service/postgres/run
RUN chmod +x /etc/service/startup/run
RUN chmod +x /etc/service/sidekiq/run
RUN chmod +x /etc/service/web/run
RUN chmod +x /etc/service/streaming/run

VOLUME /state

EXPOSE 3000 4000

LABEL maintainer="Jeffrey Paul <sneak@sneak.berlin>" \
      description="Your self-hosted, globally interconnected microblogging community"
