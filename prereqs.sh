#!/bin/bash

set -x

export DEBIAN_FRONTEND=noninteractive

MRUM="main restricted universe multiverse"
M="mirror://mirrors.ubuntu.com/mirrors.txt"
cat > /etc/apt/sources.list <<EOF
deb $M bionic $MRUM
deb $M bionic-updates $MRUM
deb $M bionic-security $MRUM
deb $M bionic-backports $MRUM
EOF

# these instructions are cribbed from
# https://docs.joinmastodon.org/administration/installation/

# this is fine, totally secure, nothing to see here
curl -sL https://deb.nodesource.com/setup_8.x | bash -

# same here
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

apt update

apt install -y \
  autoconf \
  bison \
  build-essential \
  daemontools \
  ffmpeg \
  file \
  g++ \
  gcc \
  git-core \
  imagemagick \
  libffi-dev \
  libgdbm-dev \
  libgdbm5 \
  libicu-dev \
  libidn11-dev \
  libjemalloc-dev \
  libncurses5-dev \
  libpq-dev \
  libprotobuf-dev \
  libreadline6-dev \
  libssl-dev \
  libxml2-dev \
  libxslt1-dev \
  libyaml-dev \
  mailutils \
  nginx \
  nodejs \
  pkg-config \
  postfix \
  postgresql \
  postgresql-contrib \
  protobuf-compiler \
  redis-server \
  redis-tools \
  rsync \
  yarn \
  zlib1g-dev

addgroup --gid $GID mastodon
useradd --uid $UID --gid $GID --shell /bin/bash -m -d /mastodon mastodon
